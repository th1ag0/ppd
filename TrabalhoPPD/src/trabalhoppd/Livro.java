
package trabalhoppd;

import java.util.ArrayList;

/**
 *
 * @author Thaiz
 */
public class Livro {
    private int codLivro;
    private int unidadeTempo;
    private boolean emprestado;
    private boolean lido;
    private ArrayList<Aluno> leitores;
    
    public Livro(int codLivro) {
        this.codLivro = codLivro;
        this.emprestado = false;
        if(codLivro<=5){
           unidadeTempo = 5;
        }else {
           unidadeTempo = 8;
        }
        leitores = new ArrayList<>();
    }

    public ArrayList<Aluno> getLeitores() {
        return leitores;
    }

    public void setLeitores(Aluno leitor) {
        this.leitores.add(leitor);
    }
    
    

    public int getCodLivro() {
        return codLivro;
    }

    public int getUnidadeTempo() {
        return unidadeTempo;
    }

    public boolean isEmprestado() {
        return emprestado;
    }
    public void emprestar() {
        this.emprestado = true;
    }
    
    public void ler() throws InterruptedException{
        
        String nome = Thread.currentThread().getName();
        
        System.out.println(nome + " lendo....");
        Thread.sleep(2000);
    }
    
    public String toString(){
        return "livro";
    }
}
