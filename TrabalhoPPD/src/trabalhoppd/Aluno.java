/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package trabalhoppd;

import java.util.ArrayList;
import java.util.Random;
import java.util.concurrent.Semaphore;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Thaiz
 */
public class Aluno implements Runnable {

    private int prazoEntrega;
    private int quantidade;
    private Estante estante;
    private Semaphore semaforo;
    ArrayList<Livro> livrosEmprestados;

    public Aluno(Estante estante, Semaphore semaforo) {
        this.estante = estante;
        this.quantidade = 0;
        this.semaforo = semaforo;
        this.livrosEmprestados = new ArrayList();

    }

    public void buscarLivros() throws InterruptedException {
        String nome = Thread.currentThread().getName();
        System.out.println(nome+" acessou a estante...");
        Thread.sleep(2000);
        Random gerador = new Random();
        
        boolean limite = false;
        
            for (int j = 0; j < 10; j++) {
            


                          
            int i = gerador.nextInt(10);
//                if  {
//                    
//                    System.out.println("cód: "+ estante.getLivros().get(i).getCodLivro());
//                }            
            if ((estante.getLivros().get(i).getLeitores().size() < 2 
                    && estante.getLivros().get(i).getCodLivro() > 5 
                    && !livrosEmprestados.contains(estante.getLivros().get(i)))
                    || estante.getLivros().get(i).isEmprestado() == false) {
                //   System.out.println("cód: "+ estante.getLivros().get(i).getCodLivro());
//            }
//
//            //verifica se o livro ja foi emprestado
//            if () {

                if (quantidade<5) {
                    
                    estante.getLivros().get(i).emprestar();             //EMPRESTA UM LIVRO                    
                    livrosEmprestados.add(estante.getLivros().get(i));  //ADCIONA O LIVRO EMPRESTADO A LISTA DE LIVROS EMPRESTADOS PELO ALUNO                                         
                    System.out.println(nome + " emprestou" + " livro:" + estante.getLivros().get(i).getCodLivro());                        
                    estante.getLivros().get(i).setLeitores(this);
                    quantidade++;                                       // FLAG PARA CONTABILIZAR A QUANTIDADE DE LIVROS QUE O ALUNO EMPRESTOU
                }

            } 
            if (quantidade==5) {
                limite = true;
                System.out.println(nome+" chegou no limite");
            }
        }
       
    }

    public void devolverLivro() {

    }

    @Override
    public void run() {
        while (true) {
            try {
                if (this.quantidade<5) {
                    semaforo.acquire();
                    buscarLivros();
                    semaforo.release();
                }

            } catch (InterruptedException ex) {
                Logger.getLogger(Aluno.class.getName()).log(Level.SEVERE, null, ex);
            }

        }
    }

}
