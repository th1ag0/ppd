
package trabalhoppd;

import java.util.ArrayList;
import java.util.concurrent.Semaphore;

/**
 *
 * @author Thaiz
 */
public class Principal {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        ArrayList<Livro> livro = new ArrayList();
        Semaphore semaphore = new Semaphore(1);
        Estante estante = new Estante(livro);
        
        for (int i = 0; i < 10; i++) {
            livro.add(new Livro(i+1));
        }   

        
//        for (int i = 0; i < 50; i++) {
//            
//            
//        }
        Thread aluno1 = new Thread(new Aluno(estante, semaphore), "Thiago");
        Thread aluno2 = new Thread(new Aluno(estante, semaphore), "Yasmyn");
        Thread aluno3 = new Thread(new Aluno(estante, semaphore), "João");
        Thread aluno4 = new Thread(new Aluno(estante, semaphore), "pedro");

        aluno1.start();
        aluno2.start();
        aluno3.start();
        aluno4.start();
    }

}
